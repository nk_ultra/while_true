﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoTask : PseudoBlock
{
    public TaskType task;
    public TgtType tgt;

    public void UpdateTaskInfo(){

        //construct body string
        body = "";
        //tab over appropriately
        for(int i = 0; i < numTabs; i++){
            if(i == numTabs - 1){
                body += "|-->";
            }else{
                body += "|   ";
            }
        }
        
        //say the task type first
        switch(task){
            case TaskType.None:
                body += "Error: No Task Type|";
                break;
            case TaskType.Spawn:
                body += "Spawn";
                break;
            case TaskType.Move:
                body += "Move";
                break;
        }

        //now say what it applies to
        switch(tgt){
            case TgtType.None:
                body += "|Error: No Target Type";
                break;
            case TgtType.Cookie:
                body += " cookies.";
                break;
            case TgtType.Chip:
                body += " chips.";
                break;
            case TgtType.Enemy:
                body += " enemies.";
                break;
        }
    }

    public override PseudoBlock Execute(){
        if(task == TaskType.Spawn){
            if(tgt == TgtType.None){
                Debug.Log("Error: Can't spawn TgtType None");
            }else{
                _mngr.Spawn((int)tgt);
                _mngr.SoundEffects.clip = _mngr.sfx[(int)tgt];
                _mngr.SoundEffects.Play();
            }
        }
        if(task == TaskType.Move){
            if(tgt != TgtType.Enemy){
                Debug.Log($"Error: Can only move TgtType Enemy. I was given {tgt}");
            }else{
                _mngr.MoveAllEnemies();
                _mngr.SoundEffects.clip = _mngr.sfx[(int)GameManager.sfxType.Move];
                _mngr.SoundEffects.Play();
            }
        }
        if(task == TaskType.None){
            Debug.Log("Error: TaskType None");
        }

        return nextBlock;
    }

}
