﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject whatToSpawn;
    public void Spawn(){
        //spawn your object at your position with a minor offset to randomize direction
        //(with a constant z offset of -1 to stay in front of other sprites)
        Vector3 rand = new Vector3(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f), -1);
        Instantiate(whatToSpawn, transform.position + rand , Quaternion.identity);
    }
}
