﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoComm : PseudoBlock
{
    //super easy class. A pseudo-"comment" in our code.
    //just to help player understand what's happening

    public void SetComment(string s){
        //construct body string
        body = "";
        //tab over appropriately
        for(int i = 0; i < numTabs; i++){
            if(i == numTabs - 1){
                body += "|-->";
            }else{
                body += "|   ";
            }
        }
        body += s;
    }

    public override PseudoBlock Execute(){
        return nextBlock;
    }
}
