﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    //we have a 4x4 grid of grid spots (each spot is a "room" in quotes because, well, they're not really rooms)
    Room[,] gridSpots;
    GameObject[,] cloneRoomGrid; //for destroying!
    GameObject[] spawners;
    int gridSizeX = 4;
    int gridSizeY = 4;
    public Room[] roomsToChooseFrom;
    [SerializeField] private GameObject[] interactableSpawners;
    private bool alreadyPopulated;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void PopulateGrid(){
        if(alreadyPopulated){
            DepopulateGrid();
        }
        //set up 4x4 grid for "rooms" and clones (for later destruction)
        gridSpots = new Room[gridSizeX, gridSizeY];
        cloneRoomGrid = new GameObject[gridSizeX, gridSizeY];

        //loop (OOH LIKE THE THEME) through all grid positions-- I want to fill every grid
        //spot with a"room"
        for(int i = 0; i < gridSizeX; i++){
            for(int j = 0; j < gridSizeY; j++){
                //let's find our valid room connections for this spot
                Room.RoomConnections _roomTypeAllowed = GenerateConnections(i,j);
                gridSpots[i,j] = roomsToChooseFrom[((int)_roomTypeAllowed) - 1];
                cloneRoomGrid[i,j] = Instantiate(gridSpots[i,j].gameObject,
                        new Vector2(Room.SIDE_LENGTH * j,Room.SIDE_LENGTH * (3-i)), Quaternion.identity);
            }
        }
        alreadyPopulated = true;
    }

    //commence mass destruction
    private void DepopulateGrid(){
        for(int i = 0; i < gridSizeX; i++){
            for(int j = 0; j < gridSizeY; j++){
                Destroy(cloneRoomGrid[i,j]);
            }
        }
        alreadyPopulated = false;
    }

    //we feed this function the list of all interactable spawns, and we
    //only need three distinct ones to place these guys
    public GameObject[] PlaceInteractableSpawns(GameObject[] spawns){
        int _index0 = Random.Range(0,spawns.Length);
        int _index1 = Random.Range(0,spawns.Length);

        while(spawns[_index1].transform.position.Equals(spawns[_index0].transform.position)){ //we need a distinct spot pls
            _index1 = Random.Range(0,spawns.Length);
        }

        int _index2 = Random.Range(0,spawns.Length);

        while(spawns[_index2].transform.position.Equals(spawns[_index0].transform.position) ||
                spawns[_index2].transform.position.Equals(spawns[_index1].transform.position)){ //we need a distinct spot pls
            _index2 = Random.Range(0,spawns.Length);
        }
        
        spawners = new GameObject[3];

        spawners[0] = (GameObject)Instantiate(interactableSpawners[0],
                    spawns[_index0].transform.position,Quaternion.identity);
        spawners[1] = (GameObject)Instantiate(interactableSpawners[1],
                    spawns[_index1].transform.position,Quaternion.identity);
        spawners[2] = (GameObject)Instantiate(interactableSpawners[2],
                    spawns[_index2].transform.position,Quaternion.identity);
    
        return spawners;
    }

    public void ClearInteractableSpawns(){
        if(spawners != null){
            foreach(GameObject obj in spawners){
                Destroy(obj);
            }
        }
    }

    /*
     *  Generate a room's _roomTypeAllowed variable
     *  Account for place in grid (_i, _j arguments)
     *  Chop off some connections randomly, but fairly infrequently
     */
    private Room.RoomConnections GenerateConnections(int _i, int _j){

        Room.RoomConnections _conns = Room.RoomConnections.All;
        bool _iFree = false;
        bool _jFree = false;

        //restrict _i if it's a border
        switch(_i){
            case 0:
                _conns -= Room.RoomConnections.Top;
                break;
            case 3:
                _conns -= Room.RoomConnections.Bottom;
                break;
            default:
                //no restriction-- maybe randomizable!
                _iFree = true;
                break;
        }

        //restrict _j if it's a border
        switch(_j){
            case 0:
                _conns -= Room.RoomConnections.Left;
                break;
            case 3:
                _conns -= Room.RoomConnections.Right;
                break;
            default:
                //no restriction-- maybe randomizable!
                _jFree = true;
                break;
        }

        //last chance to randomize!
        if(_iFree && _jFree){
            //we're in one of the four middle grid spots:
            //take a chance at closing one open side
            int rand = Random.Range(0,6);
            if(rand < 4){
                //if 4 or 5, don't change anything.
                //because of how the enum is set up, 2^{0,1,2,3} corresponds to
                //each individual side, so this removes only one side at random
                _conns -= (Room.RoomConnections)((int)Mathf.Pow(2.0f, (float)rand));
            }
        }

        return _conns;
    }

    /*  
     *  Return a random set of RoomConnections:
     *  Will not return RoomConnections.All
     *  Can return RoomConnections.None
     */
    private Room.RoomConnections GetRandomConnections(){
        return (Room.RoomConnections)(Random.Range(0,15));
    }
}
