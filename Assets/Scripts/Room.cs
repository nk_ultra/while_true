﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    /*
     *  So I used this enum to describe room connections for some weird reasons.
     *  Notably, it allows for us to build up connections with basic operators
     *  (for instance, All == Left + Right + Top + Bottom).
     *  This is useful for generating the room's connections-- if we start with
     *  RoomConnections.All, we can just subtract from it the specific
     *  connections we don't want (see LevelGenerator's GenerateConnections()
     *  function). None-connection "rooms" are good for calculating them, but we
     *  don't actually want to use them in the game!
     */
    public enum RoomConnections{
        None = 0,       //0000
        Left,           //0001
        Right,          //0010
        Horizontal,     //0011
        Top,            //0100
        TopLeft,        //0101
        TopRight,       //0110
        NotBottom,      //0111
        Bottom,         //1000
        BottomLeft,     //1001
        BottomRight,    //1010
        NotTop,         //1011
        Vertical,       //1100
        NotRight,       //1101
        NotLeft,        //1110
        All             //1111
    }

    public const int SIDE_LENGTH = 5; //5-unit-long sides

    [SerializeField] private RoomConnections connections;
    [SerializeField] private Vector2 gridPos;

    public Room(Vector2 _gridPosition, RoomConnections _connectionsType){
        gridPos = _gridPosition;
        connections = _connectionsType;
    }
}
