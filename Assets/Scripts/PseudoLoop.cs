﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoLoop : PseudoBlock
{
    
    public TaskType task;
    public ComparisonType comparison;
    public int exitLoopThreshold;
    public TgtType tgt;

    //nextBlock will be the first block *inside* the loop. Upon success,
    //actually go to the successBlock and continue execution. 
    public PseudoBlock exitLoopBlock;

    void UpdateLoopInfo(){

        //construct body string
        body = "";
        //tab over appropriately
        for(int i = 0; i < numTabs; i++){
            if(i == numTabs - 1){
                body += "|-->";
            }else{
                body += "|   ";
            }
        }

        //start with "While"
        body += "While";
        
        //say the task type first
        switch(task){
            case TaskType.CheckCount:
                if(tgt == TgtType.Enemy){
                    if(comparison != ComparisonType.NotEqualTo){
                        if(exitLoopThreshold == 1){
                            body += " there is";
                        }else{
                            body += " there are";
                        }
                    }else{
                        if(exitLoopThreshold == 1){
                            body += " there is not";
                        }else{
                            body += " there are not";
                        }
                    }
                }else{
                    if(comparison != ComparisonType.NotEqualTo){
                        body += " player has";
                    }else{
                        body += " player doesn't have";
                    }
                }
                break;
            default:
                Debug.Log($"Error: Attempt to while with invalid TaskType {task}");
                break;
        }

        //now the comparison type
        switch(comparison){
            case ComparisonType.LessThan:
                body += " less than";
                break;
            case ComparisonType.AtMost:
                body += " at most";
                break;
            case ComparisonType.EqualTo:
                body += " exactly";
                break;
            case ComparisonType.AtLeast:
                body += " at least";
                break;
            case ComparisonType.MoreThan:
                body += " more than";
                break;
            default:
                Debug.Log("Error: Attempt to while with undefined ComparisonType");
                break;
        }

        //the threshold for success now
        body += " ";
        body += exitLoopThreshold;

        //and now the target type
        if(exitLoopThreshold == 1){
            switch(tgt){
                case TgtType.Cookie:
                    body += " cookie:";
                    break;
                case TgtType.Chip:
                    body += " chip:";
                    break;
                case TgtType.Enemy:
                    body += " enemy:";
                    break;
                default:
                    Debug.Log("Error: Attempt to while with TgtType None");
                    break;
            }
        }else{
            switch(tgt){
                case TgtType.Cookie:
                    body += " cookies:";
                    break;
                case TgtType.Chip:
                    body += " chips:";
                    break;
                case TgtType.Enemy:
                    body += " enemies:";
                    break;
                default:
                    Debug.Log("Error: Attempt to while with TgtType None");
                    break;
            }
        }

    }

    public void SetInfo(TaskType _task, TgtType _tgt, ComparisonType _comp, int _exit){
        task = _task;
        tgt = _tgt;
        comparison = _comp;
        exitLoopThreshold = _exit;
        UpdateLoopInfo();
    }

    public override PseudoBlock Execute(){
        bool exitLoop = false;

        if(task == TaskType.CheckCount){
            exitLoop = !CheckCount();
        }

        if(!exitLoop){
            //we're going through this block (again)... let's make some updates:
            _mngr.Scroll();
            _mngr.SoundEffects.clip = _mngr.sfx[(int)GameManager.sfxType.Loop];
            _mngr.SoundEffects.Play();
        }else{
            //we're moving on! let's make some different updates:
            StartCoroutine(NewLoop());
        }

        return exitLoop ? exitLoopBlock : nextBlock;
    }

    IEnumerator NewLoop(){
        //it needs to check the loop and reset all of this during the ellipsis
        _mngr.SoundEffects.clip = _mngr.sfx[(int)GameManager.sfxType.Complete];
        _mngr.SoundEffects.Play();
        yield return new WaitForSeconds(_mngr.SecondsPerBlock*1.5f);
        _mngr.ResetStats();
        _mngr.ClearAllInteractablesFromScreen();
    }

    private bool CheckCount(){
        if(tgt == TgtType.Cookie || tgt == TgtType.Chip){
            switch(comparison){
                case ComparisonType.LessThan:
                    return _mngr.statAmounts[(int)tgt] < exitLoopThreshold;
                case ComparisonType.AtMost:
                    return _mngr.statAmounts[(int)tgt] <= exitLoopThreshold;
                case ComparisonType.NotEqualTo:
                    return _mngr.statAmounts[(int)tgt] != exitLoopThreshold;
                case ComparisonType.EqualTo:
                    return _mngr.statAmounts[(int)tgt] == exitLoopThreshold;
                case ComparisonType.AtLeast:
                    return _mngr.statAmounts[(int)tgt] >= exitLoopThreshold;
                case ComparisonType.MoreThan:
                    return _mngr.statAmounts[(int)tgt] > exitLoopThreshold;
                default:
                    Debug.Log("Error: attempt to CheckCount with invalid ComparisonType");
                    return false;
            }
        }else if(tgt == TgtType.Enemy){
            switch(comparison){
                case ComparisonType.LessThan:
                    return _mngr.activeEnemies.Length < exitLoopThreshold;
                case ComparisonType.AtMost:
                    return _mngr.activeEnemies.Length <= exitLoopThreshold;
                case ComparisonType.NotEqualTo:
                    return _mngr.activeEnemies.Length != exitLoopThreshold;
                case ComparisonType.EqualTo:
                    return _mngr.activeEnemies.Length == exitLoopThreshold;
                case ComparisonType.AtLeast:
                    return _mngr.activeEnemies.Length >= exitLoopThreshold;
                case ComparisonType.MoreThan:
                    return _mngr.activeEnemies.Length > exitLoopThreshold;
                default:
                    Debug.Log("Error: attempt to CheckCount with invalid ComparisonType");
                    return false;
            }
        }else{
            Debug.Log("Error: attempt to CheckCount with invalid TgtType");
            return false;
        }
    }

}
