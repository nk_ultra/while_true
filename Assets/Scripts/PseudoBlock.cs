﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PseudoBlock : MonoBehaviour
{

    //have to set in editor... sorry
    public GameManager _mngr;

    public enum TaskType{
        CheckCount = -2,
        None = -1,
        Spawn = 0,
        Move
    }

    public enum ComparisonType{
        LessThan = -3,
        AtMost = -2,
        NotEqualTo = -1,
        None = 0,
        EqualTo,
        AtLeast,
        MoreThan        
    }

    public enum TgtType{
        None = -1,
        Cookie = 0,
        Chip,
        Enemy
    }

    public int numTabs;

    //this is what the line should say in the loop
    //the enums actually dictate what it says
    [SerializeField] protected string body;

    [SerializeField] protected PseudoBlock nextBlock;

    public void SetNext(PseudoBlock n){
        nextBlock = n;
    }

    public PseudoBlock GetNext(){
        return nextBlock;
    }

    public string GetCode(){
        return body;
    }

    public abstract PseudoBlock Execute();

}
