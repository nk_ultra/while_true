﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopGenerator : MonoBehaviour
{
    [SerializeField] private List<PseudoTask> allTasks;
    [SerializeField] private PseudoLoop loopHeader;
    [SerializeField] private PseudoComm comment;
    [SerializeField] private List<string> spaceFillers;
    [SerializeField] private int _numLoops;

    public List<PseudoBlock> GenerateOuterLoop(int _difficulty){
        List<PseudoBlock> outerLoop = new List<PseudoBlock>();
        List<int> _missedConnections = new List<int>();
        List<int> _whileHeaders = new List<int>();

        //I did a bunch of unnecessary typecasting here, I'm pretty sure, but I
        //mostly did it just to be safe. Feel free to only typecast where
        //necessary in your own code-- I just worry a lot lol
        _numLoops = (int)(Mathf.Round(Mathf.Sqrt((float) _difficulty)) + 3);

        //add the appropriate number of loops per the equation above
        for(int i = 0; i < _numLoops; i++){
            int whileHeaderIndex = outerLoop.Count;
            outerLoop.AddRange(GenerateInnerLoop(i, _difficulty));
            _missedConnections.Add(outerLoop.Count);
            PseudoComm _endLoop = Instantiate(comment);
            _endLoop.SetComment("...");
            outerLoop.Add(_endLoop);
            _whileHeaders.Add(whileHeaderIndex);
        }
        PseudoComm endOfOuterLoop = Instantiate(comment);
        endOfOuterLoop.SetComment("Generating harder loops...");
        outerLoop.Add(endOfOuterLoop);

        //link them all together
        for(int i = 0; i < _missedConnections.Count; i++){
            //link while headers to exit blocks
            ((PseudoLoop)outerLoop[_whileHeaders[i]]).exitLoopBlock =
                            outerLoop[_missedConnections[i]];

            //link exit blocks to next PseudoBlock
            outerLoop[_missedConnections[i]].SetNext(outerLoop[_missedConnections[i] + 1]);
        }

        return outerLoop;
    }

    private List<PseudoBlock> GenerateInnerLoop(int _pos, int _diff){
        if(_pos == 0){
            return GenerateLoopZero(_diff);
        }else{
            return GenerateSingleLoop(_diff);
        }
    }

    //loop zero will be unloseable*; it's just to get you used to the new speed
    // *exception: sometimes they will spawn enemies and you could die
    //loop zero will only spawn as the first loop of each outer loop
    private List<PseudoBlock> GenerateLoopZero(int _diff){
        List<PseudoBlock> loop = new List<PseudoBlock>();

        //we hardcode the very first one because we are nice to the players
        if(_diff == 1){

            //set up blocks
            PseudoLoop header = Instantiate(loopHeader);
            header.SetInfo(PseudoBlock.TaskType.CheckCount, PseudoBlock.TgtType.Cookie,
                        PseudoBlock.ComparisonType.LessThan, 2);

            PseudoComm comm0 = Instantiate(comment);
            comm0.numTabs = 1;
            comm0.SetComment("// You can't exit this loop unless");

            PseudoComm comm1 = Instantiate(comment);
            comm1.numTabs = 1;
            comm1.SetComment("// you break the condition above!");

            PseudoTask spawnCookie = Instantiate(allTasks[0]);
            spawnCookie.numTabs = 1;
            spawnCookie.UpdateTaskInfo();

            //make connections
            header.SetNext(comm0);
            comm0.SetNext(comm1);
            comm1.SetNext(spawnCookie);
            spawnCookie.SetNext(header);

            //ensure manager is added
            header._mngr = GameManager.Instance;
            comm0._mngr = GameManager.Instance;
            comm1._mngr = GameManager.Instance;
            spawnCookie._mngr = GameManager.Instance;

            //add it all to our list
            loop.Add(header);
            loop.Add(comm0);
            loop.Add(comm1);
            loop.Add(spawnCookie);

        }else{
            loop = GenerateSingleLoop(_diff);
        }

        return loop;
    }

    private List<PseudoBlock> GenerateSingleLoop(int _diff){
        List<PseudoBlock> loop = new List<PseudoBlock>();

        //requirement to break out of the loop (0,1) - removed 2 (# enemies)
        // because it often got skipped after spawning many
        int _loopReq = Random.Range(0, 2);
        
        //the "guts" of the loop, aka the code inside of it
        //the method it calls? absolutely disgusting imo
        List<PseudoBlock> _loopGuts = GenerateTaskSequence(_diff, 3, _loopReq, 1);

        PseudoLoop _header = Instantiate(loopHeader);
        _header.SetInfo(PseudoBlock.TaskType.CheckCount, (PseudoBlock.TgtType)_loopReq,
                    PseudoBlock.ComparisonType.LessThan, Random.Range(2,_numLoops*3/2));

        loop.Add(_header);
        loop.AddRange(_loopGuts);
        FinalizeLoop(loop);
        return loop;
    }

    private List<PseudoBlock> GenerateTaskSequence(int _diff, int _min,
                                                        int _reqSpawn, int _tabs){
        List<PseudoBlock> _seq = new List<PseudoBlock>();
        bool[] flags = new bool[]{false, false, false, false};

        //weirdly good measurement for how many tasks in a row is too much at a difficulty:
        //the number of loops being executed
        int _size = Random.Range(_min, _numLoops + 1);

        for(int i = 0; i < _size; i++){
            //if a random float is bigger than some difficulty math, instantiate "move
            //enemy" task. If not, pick a random harmless task.
            PseudoTask task = Random.Range(0.0f, 1.0f) > (2.0f/((float)_diff + 3.0f) + 0.15) ?
                    Instantiate(allTasks[3]) : PickRandomHarmlessTask();

            //update our flags
            if(task.tgt == PseudoBlock.TgtType.Enemy){
                //move enemy, update appropriate flag
                if(Random.Range(0.0f,1.0f) > 0.75f){
                    //random chance to change it to a spawn instead
                    flags[2] = true;
                    task = Instantiate(allTasks[2]);
                }else{
                    flags[3] = true;
                }
            }else if(task.task == PseudoBlock.TaskType.Spawn){
                //we've got a regular spawn, update appropriate flag
                flags[(int)task.tgt] = true;
            }

            if(flags[3] && !flags[2]){
                //we're trying to move enemies without spawning them!
                task = Instantiate(allTasks[2]);
                flags[2] = true;
            }

            //now add it to our sequence
            task.numTabs = _tabs;
            _seq.Add(task);
        }

        //we have a base sequence. let's check how it fits the requirements.
        if(_reqSpawn > -1 && _reqSpawn < 3){
            //only 0, 1, 2 are valid spawn requirements (for cookie, chip, enemy respectively)
            if(!flags[_reqSpawn]){
                //we're missing a requirement! replace the last element with it!
                _seq.Remove(_seq[_seq.Count - 1]);
                PseudoTask task = Instantiate(allTasks[_reqSpawn]);
                task.numTabs = _tabs;
                _seq.Add(task);
            }
        }
        if(_diff > 2){
            int rand = Random.Range(0, _seq.Count * 2 + _diff);
            if(rand < _seq.Count){
                //lucky you! let's put in a spacefiller comment
                PseudoComm filler = Instantiate(comment);
                filler.SetComment(spaceFillers[Random.Range(0,spaceFillers.Count)]);
                _seq.Insert(rand,filler);
            }
        }

        return _seq;
    } 

    private PseudoTask PickRandomHarmlessTask(){
        //either spawn cookie or spawn chip
        PseudoTask task = Random.Range(0.0f, 1.0f) < 0.5f ? Instantiate(allTasks[0]) :
                                                            Instantiate(allTasks[1]);
        return task;
    }

    /*
     *  What is the difference between a list of PseudoBlocks and a bona-fide executable
     *  Pseudo-While-Loop? Well, the FinalizeLoop method converts a regular list into the
     *  loopable structure. Three things happen here for each element:
     *      1. It fills its _mngr slot
     *      2. It is appropriately linked up to its next one  
     *      3. It updates its info so it can display text on-screen
     */
    private void FinalizeLoop(List<PseudoBlock> list){
        for(int i = 0; i < list.Count; i++){
            //give them each the manager they need
            list[i]._mngr = GameManager.Instance;

            //each block's next block is next to it in the list, and the % list.Count is for
            //looping back around to the beginning of the list for the last element
            list[i].SetNext(list[(i+1) % list.Count]);

            //update task info
            if(list[i] is PseudoTask){
                ((PseudoTask)list[i]).UpdateTaskInfo();
            }
        }
    }
}
