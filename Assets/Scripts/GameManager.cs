﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

/*
 *  This class will be our main manager for our game
 *  (hence why it is aptly named GameManager).
 *  It will be used by various other scripts to get
 *  various global things like the player or UI.
 *  
 *  This class will also function as our "Master Loop"
 *  that executes each loop the player sees and
 *  increases in difficulty with each loop.
 *  
 *  Since there's really only one scene, it won't
 *  have to be a singleton!
 */
public class GameManager : MonoBehaviour
{
    //Our manager
    public static GameManager Instance;

    //Our player
    public GameObject PlayerObj;
    public bool PlayerAlive;
    
    //Loop information + difficulty
    public int Difficulty;
    public LoopGenerator LoopGen;
    public List<PseudoBlock> PseudoBlocks;
    public int currLine;
    public float SecondsPerBlock;
    public bool Executing;

    //Our level generator and info
    public LevelGenerator LvlGen;
    private GameObject[] spawners;
    public GameObject[] activeEnemies;

    //Our Gameplay UI
    public List<TextMeshProUGUI> LoopTextLines;
    public LineGenerator LineGen;
    private Vector2 origLineGenPos;
    public TextMeshProUGUI StatsText;
    [SerializeField] private string[] statNames;
    public int[] statAmounts;
    private int[] statTotals;


    //our audio
    public AudioSource SoundEffects;
    public enum sfxType{
        Cookie = 0,
        Chip,
        Enemy,
        Move,
        Loop,
        Complete
    }
    public AudioClip[] sfx;
    public AudioSource Music;
    public AudioClip soundtrack;

    void Awake()
    {
        Instance = this;

        //debugging for common errors due to developer forgetfulness
        PlayerObj = GameObject.FindGameObjectWithTag("Player");
        Executing = false;
        origLineGenPos = LineGen.gameObject.transform.position;
        string statsText = "";
        statTotals = new int[]{0,0,0};
        for(int i = 0; i < statAmounts.Length; i++){
            statsText = statsText + statNames[i] + ": " + statAmounts[i] + "\n";
        }
        SoundEffects.volume = 0.5f;
        StatsText.text = statsText;
        StartNewLoop();
    }

    void StartNewLoop(){

        //remove leftovers from last round if applicable
        LvlGen.ClearInteractableSpawns();
        ClearAllEnemies();
        GameObject[] _leftoverBlox = GameObject.FindGameObjectsWithTag("Block");
        foreach(GameObject obj in _leftoverBlox){
            Destroy(obj);
        }

        //tell our level generator to regenerate a level
        LvlGen.PopulateGrid();

        //Difficulty == Outer loop #
        Difficulty++;
        CalculateSecsPerBlock();

        //randomly place some interactable spawners
        GameObject[] interactableSpawns = GameObject.FindGameObjectsWithTag("InteractableSpawn");
        spawners = LvlGen.PlaceInteractableSpawns(interactableSpawns);
        
        //reset code line text
        foreach(TextMeshProUGUI _tmpugui in LoopTextLines){
            Destroy(_tmpugui);
        }
        LineGen.gameObject.transform.position = origLineGenPos;

        //generate some pseudocode!
        PseudoBlocks = LoopGen.GenerateOuterLoop(Difficulty);
        LoopTextLines = LineGen.GenerateLoopText(PseudoBlocks);

        PlayerObj.GetComponent<Player>().ResetPos();

        //execute that stuff!
        StartCoroutine(ExecuteLoop());
    }

    IEnumerator ExecuteLoop(){
        PseudoBlock block = PseudoBlocks[0];
        Executing = true;
        while(block != null){
            currLine = PseudoBlocks.IndexOf(block);
            Color orig = HighlightLine(currLine, Color.red);
            block = block.Execute();
            activeEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            yield return new WaitForSeconds(SecondsPerBlock);
            HighlightLine(currLine, orig);
        }
        //we've hit null: we need to do the next loop
        Executing = false;
        StartNewLoop();
    }

    Color HighlightLine(int _index, Color _newColor){
        Color oldCol = LoopTextLines[_index].color;
        LoopTextLines[_index].color = _newColor;
        return oldCol;
    }

    //gross math that dictates how many seconds per block we have at a given difficulty
    //actual equation: 2/(sqrt[Difficulty^2 + 1])
    //will never be zero, but will continually get smaller
    private void CalculateSecsPerBlock(){
        float _sqrtExpr = Mathf.Sqrt(Mathf.Pow(Difficulty, 2.0f) + 1.0f);
        SecondsPerBlock = 2.0f/_sqrtExpr;
        if(SecondsPerBlock == 0.0f){
            //congrats! you broke my float! have the smallest possible slice we can give you
            //it's never gonna get here, but *just in case*
            SecondsPerBlock = float.Epsilon;
        }
    }

    //I don't use this anymore but it was useful for debugging
    private void TellAllSpawnersToSpawn(){
        foreach(GameObject obj in spawners){
            obj.GetComponent<Spawner>().Spawn();
        }
    }

    public void Spawn(int whatToSpawn){
        spawners[whatToSpawn].GetComponent<Spawner>().Spawn();
    }

    public void MoveAllEnemies(){
        foreach(GameObject obj in activeEnemies){
            obj.GetComponent<Enemy>().MoveEnemy();
        }
    }

    private void ClearAllEnemies(){
        foreach(GameObject obj in activeEnemies){
            Destroy(obj);
        }
    }

    //Update stats panel
    public void UpdateStatsPanel(int statNum, int statIncrement){
        statAmounts[statNum] += statIncrement;
        statTotals[statNum] += statIncrement;
        string statsText = "";
        for(int i = 0; i < statAmounts.Length; i++){
            statsText = statsText + statNames[i] + ": " + statAmounts[i] + "\n";
        }
        StatsText.text = statsText;
    }

    public void ResetStats(){
        string statsText = "";
        for(int i = 0; i < statAmounts.Length; i++){
            statAmounts[i] = 0;
            statsText = statsText + statNames[i] + ": " + statAmounts[i] + "\n";
        }
        StatsText.text = statsText;
    }

    public void ClearAllInteractablesFromScreen(){
        GameObject[] allInteractables = GameObject.FindGameObjectsWithTag("Interactable");
        foreach(GameObject obj in allInteractables){
            Destroy(obj);
        }
    }

    public void PlayerDie(){
        StopAllCoroutines();
        SceneManager.LoadScene("YouHaveDied");
    }

    public void Scroll(){
        StartCoroutine(ScrollUpLines());
    }

    IEnumerator ScrollUpLines(){
        Vector2 lineGenPos = LineGen.gameObject.transform.position;
        Vector2 dest = origLineGenPos + new Vector2(0, currLine * 0.95f);

        while(Vector2.Distance(LineGen.gameObject.transform.position, dest) > 0.05f){
            LineGen.gameObject.transform.position = Vector2.Lerp(
                LineGen.gameObject.transform.position, dest, 5.0f * Time.deltaTime);
            yield return null;
        }
    }

}
