﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //to avoid making a bunch of static stuff, here's a reference to the GameManager
    [SerializeField] private GameManager _mngr;

    //movement variables
    public float moveSpeed = 5f;
    public Rigidbody2D rb;
    Vector2 movement;

    //interaction variables
    [SerializeField] private bool canInteract;
    [SerializeField] private List<GameObject> range;

    // Start is called before the first frame update
    void Start()
    {
        _mngr = GameManager.Instance;
        canInteract = false;
        range = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        //first up: get movement
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //now check for interactions
        if(Input.GetButtonDown("Interact")){
            Interact();
        }
    }
    void FixedUpdate(){
        rb.MovePosition(rb.position + movement.normalized * moveSpeed * Time.fixedDeltaTime);
    }

    void OnCollisionEnter2D(Collision2D col){
        if(col.collider.gameObject.tag == "Enemy"){
            _mngr.PlayerDie();
        }
    }

    void OnTriggerEnter2D(Collider2D c){
        if(c.gameObject.tag == "Interactable"){
            canInteract = true;
            range.Add(c.gameObject);
            c.gameObject.GetComponent<Interactable>().Sparkle(true);
        }
    }

    void OnTriggerExit2D(Collider2D c){
        if(range.Contains(c.gameObject)){
            c.gameObject.GetComponent<Interactable>().Sparkle(false);
            range.Remove(c.gameObject);
            canInteract = range.Count > 0;
        }
    }

    void Interact(){
        if(canInteract){
            foreach(GameObject obj in range){
                int[] statUpdate = obj.GetComponent<Interactable>().Interact();
                _mngr.UpdateStatsPanel(statUpdate[0], statUpdate[1]);
            }
            for(int i = range.Count-1; i >= 0; i--){
                Destroy(range[i]);
            }
            range.RemoveRange(0, range.Count);
        }
    }

    public void ResetPos(){
        transform.position = new Vector2(0,0);
    }
}
