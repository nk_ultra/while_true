﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawn : MonoBehaviour
{
    public GameObject[] spawnableBlocks;

    // Start is called before the first frame update
    void Start()
    {
        int rand = Random.Range(0, spawnableBlocks.Length);
        Instantiate(spawnableBlocks[rand], transform.position, Quaternion.identity);
    }
}
