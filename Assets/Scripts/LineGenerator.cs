﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LineGenerator : MonoBehaviour
{
    
    [SerializeField] private GameObject line;

    public List<TextMeshProUGUI> GenerateLoopText(List<PseudoBlock> list){
        List<TextMeshProUGUI> lines = new List<TextMeshProUGUI>();
        int currIndex = 0;
        foreach(PseudoBlock pBlock in list){

            //starting point for on-screen pseudocode
            //since its transform's parent will be set to the generator's transform,
            //we need its y value to decrement in groups of 40 to grow downward from
            //the top
            Vector2 spawnPos = new Vector2(0.0f, currIndex * -30.0f);

            //instantiate the actual line
            GameObject obj = Instantiate(line, spawnPos, Quaternion.identity);
            obj.transform.SetParent(transform, false);

            //insert its TMPUGUI into the list and set the text accordingly
            lines.Insert(currIndex, obj.GetComponent<TextMeshProUGUI>());
            lines[currIndex].text = pBlock.GetCode();

            if(pBlock is PseudoComm){ //comments need to be less bright
                lines[currIndex].color = new Color(0.6f,0.65f,0.6f,0.8f);
            }
            
            //don't forget to increment!
            currIndex++;
        }
        return lines;
    }

}
