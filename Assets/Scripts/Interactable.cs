﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    //due to worries about time I've opted to use bools
    //for types instead of subclasses
    //true if product, false if consumer
    [SerializeField] private bool isProduct;

    //as of right now, this either interacts with cookies or chips
    //will change if necessary
    [SerializeField] private bool dealsWithCookie;

    /*
     * public Interact method
     * returns an int array of form
     * [<number of stat to change>, <increment/decrement amount>]
     */
    public int[] Interact(){
        int[] statUpdate = new int[]{0,0};
        //set our stat number to the type
        statUpdate[0] = dealsWithCookie ? 0 : 1;
        //set our increment to +1 if this is a product and -1 if it consumes a product
        statUpdate[1] = isProduct ? 1 : -1;
        return statUpdate;
    } 

    public void Sparkle(bool _enabled){
        
    }
}
