﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameManager _mngr;

    void Start(){
        _mngr = GameManager.Instance;
    }

    public void MoveEnemy(){
        int randX = Random.Range(-1,2); //-1, 0, or 1
        int randY = Random.Range(-1,2); //ditto
        Vector2 desiredMovement = (Vector2)transform.position + new Vector2(randX,randY);
        StartCoroutine(SmoothMove(desiredMovement));
    }

    IEnumerator SmoothMove(Vector2 _newPos){
        float _timeElapsed = 0.0f;
        while(Vector2.Distance(transform.position, _newPos) > 0.05f
                && _timeElapsed < _mngr.SecondsPerBlock){
            //while we're still far from our goal and we're still executing:
            //lerp us to our goal position-- 5.0f is a magic number but it makes the
            //movement nice and snappy
            transform.position = Vector2.Lerp(transform.position, _newPos, 5.0f*Time.deltaTime);
            _timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}
